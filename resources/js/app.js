require('./bootstrap');
import Vue from 'vue'


import VueRouter from 'vue-router'
Vue.use(VueRouter)



//import Vuetify from 'vuetify'
//Vue.use(Vuetify)
import Index from './components/Index'
import Get from './components/Get'
import Login from './components/Login'
import Personal from './components/Personal'
import Registration from './components/Registration'
import App from './components/App'

const router = new VueRouter({
	mode: 'history',
	routes: 
	[
			{
			  path: '/get',
			  component: Get,
			  name: 'get.index'
			},
			 {
			  path: '/user/login',
			  component: Login,
			  name: 'user.login'
			},
			 {
			  path: '/user/registration',
			  component: Registration,
			  name: 'user.registration'
			},
			{
			  path: '/user/index',
			  component: Index,
			  name: 'user.index'
			},
			{
			  path: '/user/personal',
			  component: Personal,
			  name: 'user.personal'
			},
		

	]
})


//https://russianblogs.com/article/99131425166/
router.beforeEach((to, from, next) => {
  const token= localStorage.getItem('x_xsrf_token')
	  if(!token ){
	  		if(to.name === 'user.login' || to.name === 'user.registration'){
	  			return next()
	  		}else{
	  			return next({
	  				name: 'user.login'
	  			})	
	  		}
	  }

	  if(to.name === 'user.login' || to.name === 'user.registration' && token ){
	  		return next ( {name: 'user.personal'})
	  }

	  next()
})




const app = new Vue({
    el: '#app',
    components: { App }, 
    router
    //vuetify: new Vuetify()
})
